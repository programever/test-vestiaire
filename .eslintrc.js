module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint'],
  rules: {
    'space-before-function-paren': 'off',
    'no-spaced-func': 'off',
    'operator-linebreak': 'off',
    'no-throw-literal': 'off',
    'promise/avoid-new': 'off',
    semi: 'off',
  },
}
