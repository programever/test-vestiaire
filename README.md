## Read Me

Gitlab repo: https://gitlab.com/programever/test-vestiaire/

Live demo using Gitlab Pipeline: https://programever.gitlab.io/test-vestiaire/

The solution for `Development` present at src/View.tsx which used src/Functions.ts - This is where all the functions stay

The answer for `Questions` present at src/View.tsx which used src/QuestionAnswer.ts - I recommend you see from website is easier

To run website in localhost: `npm run start`

To test: `npm run test`

To watch eslint: `npm run lint`

To build: `npm run build`

## I - Development

Given the products.json that is on this project which represents a response from a getProducts() API call
Please provide a solution for each questions that follows with the stack/framework/language of your choice
Each question is independent from the others.

Comments are appreciated ;-)

1. User is coming from an Off-White promotion offer link, display only the Off-White's products with a reduced price of 10%.

2. Louis Vuitton doesn't want us to display the name of their brand on our website, could you reverse the name of the brand for each LV product to obfuscate their name ?

3. I'm a user from UK and I want to see product between 1500€ and 500€, ordered from the cheaper to the most expensive that are shippable to my country.

4. We want to display how many days/month/year since each products has been deposited on the website (ie: Deposited 1month and 3days ago)

## II - Questions

There are no wrong answers, only good opportunities to learn something new.

1. What metrics are essential in term of Speed ?

2. Can you name ways to increase speed (perceived or actual load time) ?

3. Could you tell me what are SSR, pre-rendering and Dynamic rendering ?

4. You have a bug to fix, you find the file(s) where the bug occurs, the code is a mess, what do you do ?

5. What represent FrontEnd to you ?

6. What was the last technical challenge you faced and how you did you handle it ?

7. What is the next language/framework/stack you want to learn this year and why ?

