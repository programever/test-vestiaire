module.exports = {
  testRegex: ["\\.spec\\.ts$"],
  moduleFileExtensions: ["js", "json", "jsx", "ts", "tsx", "node"],
  preset: "ts-jest",
};
