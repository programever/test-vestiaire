import * as Fn from "../src/Functions";
import { Product } from "../src/Types";
import { response } from "./Fixture";

describe("Functions", () => {
  const products: Product[] = response.products;

  it("filterByBrand", async () => {
    const brandName = "Off-White";

    const xs = products.filter(Fn.filterByBrand(brandName));

    xs.forEach((x) => {
      expect(x.brand).toBe(brandName);
    });
  });

  it("filterByPrice", async () => {
    const min = 500;
    const max = 1500;

    const xs = products.filter(Fn.filterByPriceRange(min, max));

    xs.forEach((x) => {
      expect(min * 100).toBeLessThanOrEqual(x.price.price_in_cents);
      expect(max * 100).toBeGreaterThanOrEqual(x.price.price_in_cents);
    });
  });

  it("filterByShippable", async () => {
    const country = "UK";

    const xs = products.filter(Fn.filterByShippable(country));

    xs.forEach((x) => {
      expect(x.shippable_countries.includes(country)).toBe(true);
    });
  });

  it("discountByPct", async () => {
    const pct = -10;

    const xs = products.map(Fn.discountByPct(pct));

    xs.forEach((x, index) => {
      const original = products[index];
      const newPrice =
        original.price.price_in_cents +
        (pct * original.price.price_in_cents) / 100;

      expect(x.price.price_in_cents).toBe(newPrice);
    });
  });

  it("reverseBrand", async () => {
    const brandName = "Louis Vuitton";
    const xs = products.map(Fn.reverseBrand(brandName));

    xs.forEach((x, index) => {
      const original = products[index];

      if (original.brand === brandName) {
        expect(x.brand).toBe("nottiuV siuoL");
      } else {
        expect(x.brand).toBe(original.brand);
      }
    });
  });

  it("formatDeposited - formatYMDAgo", async () => {
    // Because of testing current date is not accurate with the provided fixture
    // So we will test the internal function instead

    const month = 30;
    const _2months = month * 2;
    const year = 12 * month;
    const _2years = year * 2;

    const today = Fn.formatYMDAgo(new Date().toISOString());
    expect(today).toBe("Today");

    const _1dayAgo = Fn.formatYMDAgo(dayAgo(1));
    expect(_1dayAgo).toBe("1 day ago");

    const _2daysAgo = Fn.formatYMDAgo(dayAgo(2));
    expect(_2daysAgo).toBe("2 days ago");

    const _1monthAgo = Fn.formatYMDAgo(dayAgo(month));
    expect(_1monthAgo).toBe("1 month ago");

    const _2monthsAgo = Fn.formatYMDAgo(dayAgo(_2months));
    expect(_2monthsAgo).toBe("2 months ago");

    const _1month1dayAgo = Fn.formatYMDAgo(dayAgo(month + 1));
    expect(_1month1dayAgo).toBe("1 month and 1 day ago");

    const _1month2daysAgo = Fn.formatYMDAgo(dayAgo(month + 2));
    expect(_1month2daysAgo).toBe("1 month and 2 days ago");

    const _2months1dayAgo = Fn.formatYMDAgo(dayAgo(_2months + 1));
    expect(_2months1dayAgo).toBe("2 months and 1 day ago");

    const _2months2daysAgo = Fn.formatYMDAgo(dayAgo(_2months + 2));
    expect(_2months2daysAgo).toBe("2 months and 2 days ago");

    const _1yearAgo = Fn.formatYMDAgo(dayAgo(year));
    expect(_1yearAgo).toBe("1 year ago");

    const _2yearsAgo = Fn.formatYMDAgo(dayAgo(_2years));
    expect(_2yearsAgo).toBe("2 years ago");

    const _1year1monthAgo = Fn.formatYMDAgo(dayAgo(year + month));
    expect(_1year1monthAgo).toBe("1 year and 1 month ago");

    const _1year1month1dayAgo = Fn.formatYMDAgo(dayAgo(year + month + 1));
    expect(_1year1month1dayAgo).toBe("1 year 1 month and 1 day ago");

    const _1year2monthsAgo = Fn.formatYMDAgo(dayAgo(year + _2months));
    expect(_1year2monthsAgo).toBe("1 year and 2 months ago");

    const _1year2months1dayAgo = Fn.formatYMDAgo(dayAgo(year + _2months + 1));
    expect(_1year2months1dayAgo).toBe("1 year 2 months and 1 day ago");

    const _1year2months2daysAgo = Fn.formatYMDAgo(dayAgo(year + _2months + 2));
    expect(_1year2months2daysAgo).toBe("1 year 2 months and 2 days ago");

    const _2years2months2daysAgo = Fn.formatYMDAgo(
      dayAgo(_2years + _2months + 2)
    );
    expect(_2years2months2daysAgo).toBe("2 years 2 months and 2 days ago");
  });

  it("sortPriceAsc", async () => {
    const xs = products.sort(Fn.sortPriceAsc);

    xs.forEach((x, index) => {
      const lastPrice =
        index === 0 ? 0 : products[index - 1].price.price_in_cents;
      expect(lastPrice).toBeLessThan(x.price.price_in_cents);
    });
  });

  it("filter brand and reduce price 10%", async () => {
    const brandName = "Off-White";
    const pct = -10;

    const originalXs = products.filter(Fn.filterByBrand(brandName));
    const xs = Fn.query(
      Fn.filterByBrand(brandName),
      Fn.discountByPct(pct),
      null,
      products
    );

    for (let i = 0; i < xs.length; i++) {
      const originalX = originalXs[i];
      const x = xs[i];

      const newPrice =
        originalX.price.price_in_cents +
        (pct * originalX.price.price_in_cents) / 100;

      // Test brand name
      expect(x.brand).toBe(brandName);
      // Test price
      expect(x.price.price_in_cents).toBe(newPrice);
    }
  });

  it("reverse brand name", async () => {
    const brandName = "Louis Vuitton";

    const xs = products.map(Fn.reverseBrand(brandName));

    for (let i = 0; i < products.length; i++) {
      const original = products[i];
      const x = xs[i];

      if (original.brand === brandName) {
        // Test reverse brand name
        expect(x.brand).toBe("nottiuV siuoL");
      } else {
        // Test same brand name
        expect(x.brand).toBe(original.brand);
      }
    }
  });

  it("filter UK and price from 500 - 1500 and order price asc", async () => {
    const country = "UK";
    const min = 500;
    const max = 1500;

    const filtering = (product: Product): boolean =>
      Fn.filterByShippable(country)(product) &&
      Fn.filterByPriceRange(min, max)(product);
    const xs = Fn.query(filtering, null, Fn.sortPriceAsc, products);

    for (let i = 0; i < xs.length; i++) {
      const x = xs[i];
      const lastPrice = i === 0 ? 0 : xs[i - 1].price.price_in_cents;

      // Test shippable
      expect(x.shippable_countries.includes(country)).toBe(true);
      // Test price range
      expect(min * 100).toBeLessThanOrEqual(x.price.price_in_cents);
      expect(max * 100).toBeGreaterThanOrEqual(x.price.price_in_cents);
      // Test ordering
      expect(lastPrice).toBeLessThan(x.price.price_in_cents);
    }
  });
});

function dayAgo(day: number): string {
  const dayInMillis = day * 24 * 60 * 60 * 1000;
  const millis = new Date().getTime();
  return new Date(millis - dayInMillis).toISOString();
}
