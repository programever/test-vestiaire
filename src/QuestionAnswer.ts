import { QuestionAnswer } from "Types";

const items: QuestionAnswer[] = [
  {
    question: "1. What metrics are essential in term of Speed ?",
    answer: `
- The most important are Time To First Byte and Page Load Time, because these are 2 things that we can improve/optimize as a developer.

- The rest of the metrics require Devops to do like TCP Connection Time, Page Response Time...
`,
  },
  {
    question:
      "2. Can you name ways to increase speed (perceived or actual load time) ?",
    answer: `
- Optimize backend code: always update dependencies, use async operations, use streaming for big data, prevent memory leak

- Optimize frontend code: always update dependencies, use Parcel/Webpack/LightningCSS, optimize images/assets, use lazy loadding/Code-Splitting, prevent re-render

- Caching and Optimize for DB query

- Data transfer should be compressed

- Use reverse proxy, HTTP/2, load balancer and CDN
`,
  },
  {
    question:
      "3. Could you tell me what are SSR, pre-rendering and Dynamic rendering ?",
    answer: `
- SSR: Server will render HTML and send it to browser, this is like PHP/Ruby way of serving website.

- Pre-rendering: Technically, this one is like static HTML, because it was generated while build time.

- Dynamic rendering: Base on the request's user agent, server will serve different version of web page.
`,
  },
  {
    question:
      "4. You have a bug to fix, you find the file(s) where the bug occurs, the code is a mess, what do you do ?",
    answer: `
- Git checkout to new branch first, no matter what I do, it did not harm others.

- Most of mess code come with unformated/no-prettier, make it pretty will help alot.

- Making sure that I can go to reference/definition using my editor, this will help me move around faster.

- Base on the programming language, find the root problem by using: logging, profiling, or whatever that I can trace to the root problem.

- After find out the root problem, check why test cases not cover this bug if existed, discuss with the creator of this bug if able to.

- While fixing the bug, try to prevent "quick fix", lets do it properly if not "karma" will come again! Try to get advice from creator if able to.

- Write test case if needed, and ask Tester/QA/Creator to test this bug again.
`,
  },
  {
    question: "5. What represent FrontEnd to you ?",
    answer: `
- Zero runtime error
- UI must be modern
- UX must be friendly
- Animation must be smooth
- User Flow must be logic
`,
  },
  {
    question:
      "6. What was the last technical challenge you faced and how you did you handle it ?",
    answer: `
- The context is we are creating Tailwind CSS package for PureScript https://github.com/haniker-dev/purescript-tailwind-css

- This package will help you use Tailwind class names with type-safe and also generate CSS at build time.

- We need to gather all the element class names under build time, so that we can ask Tailwind to scan and generate CSS base on those class names.

- We have tried many many ways, but fail! In the end we found that, at build time, only 1 thing can help us is compiler.

- It lead us to use Symbol! With Symbol, we can access to compiler level and from there we can solve our problem using Append class from Data.Symbol

- Sound is easy, but it took us like more than 1 month to come out with this solution!
`,
  },
  {
    question:
      "7. What is the next language/framework/stack you want to learn this year and why ?",
    answer: `
- PureScript programming language - Because the TypeClass of Functional Programming is very big, I need to understand and master them.

- I also want to contribute to PureScript community like: Create packages, introduce PureScript to my co-worker, etc...
`,
  },
];

export default items;
