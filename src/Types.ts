export type QuestionAnswer = { question: string; answer: string };

export type Response = { products: Product[] };

export type Product = {
  id: number;
  name: string;
  brand: string;
  seller: Seller;
  price: Price;
  deposited_on: string;
  shippable_countries: string[];
};

export type Seller = {
  name: string;
  id: number;
  country: string;
};

export type Price = {
  currency: string;
  price_in_cents: number;
  price: string;
};
