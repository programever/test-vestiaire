import { ReactElement } from "react";
import { Product } from "./Types";
import response from "./Response";
import questionAnswer from "./QuestionAnswer";
import * as Fn from "./Functions";

export default function view(): ReactElement {
  return (
    <div className="container mx-auto">
      {developmentView()}
      {questionsView()}
      {thankYouView()}
    </div>
  );
}

function developmentView(): ReactElement {
  const { products } = response;

  const offWhites = Fn.query(
    Fn.filterByBrand("Off-White"),
    Fn.discountByPct(-10),
    null,
    products
  );

  const lvs = products.map(Fn.reverseBrand("Louis Vuitton"));

  const ukFiltering = (product: Product): boolean =>
    Fn.filterByShippable("UK")(product) &&
    Fn.filterByPriceRange(500, 1500)(product);
  const uks = Fn.query(ukFiltering, null, Fn.sortPriceAsc, products);

  const formatedDepositedOn = products.map(Fn.formatDepositedOn);

  return (
    <div className="container mx-auto">
      {titleView("Development")}
      {questionView(
        "1. User is coming from an Off-White promotion offer link, display only the Off-White's products with a reduced price of 10%."
      )}
      {productsView(offWhites)}
      {questionView(
        "2. Louis Vuitton doesn't want us to display the name of their brand on our website, could you reverse the name of the brand for each LV product to obfuscate their name ?"
      )}
      {productsView(lvs)}
      {questionView(
        "3. I'm a user from UK and I want to see product between 1500€ and 500€, ordered from the cheaper to the most expensive that are shippable to my country."
      )}
      {productsView(uks)}
      {questionView(
        "4. We want to display how many days/month/year since each products has been deposited on the website (ie: Deposited 1month and 3days ago)"
      )}
      {productsView(formatedDepositedOn)}
    </div>
  );
}

function questionsView(): ReactElement {
  return (
    <div className="container mx-auto">
      {titleView("Questions")}
      {questionAnswer.map(({ question, answer }) => {
        return (
          <div key={question}>
            {questionView(question)}
            {answerView(answer)}
          </div>
        );
      })}
    </div>
  );
}

function titleView(s: string): ReactElement {
  return <div className="text-3xl font-bold my-8">{s}</div>;
}

function questionView(s: string): ReactElement {
  return <div className="text-lg font-bold">{s}</div>;
}

function answerView(s: string): ReactElement {
  return <div className="mb-8 whitespace-pre-wrap">{s}</div>;
}

function thankYouView(): ReactElement {
  return (
    <div className="mt-10">
      {questionView("Comments:")}
      <div className="pb-20 whitespace-pre-wrap">
        {`
- I think https://us.vestiairecollective.com/ using NextJS, NextJS have Streaming SSR and Server Component, I hope that you are using this in your project.

- Graphql also a good choice for your concept, because it allow frontend to request to public database without creating API endpoint under backend. Eg: https://opensea.io/

- Have you use https://lightningcss.dev/ ?

- Last but not least, THANK YOU for your time, have a great day ahead!
`}
      </div>
    </div>
  );
}

function productsView(products: Product[]): ReactElement {
  return (
    <div className="overflow-auto max-h-[20rem] border mt-2 mb-10">
      <table className="min-w-full text-left text-sm font-light">
        <thead className="border-b font-medium dark:border-neutral-500">
          <tr>
            <th scope="col" className="px-6 py-4">
              #
            </th>
            <th scope="col" className="px-6 py-4">
              Name
            </th>
            <th scope="col" className="px-6 py-4">
              Brand
            </th>
            <th scope="col" className="px-6 py-4">
              Price
            </th>
            <th scope="col" className="px-6 py-4">
              Country
            </th>
            <th scope="col" className="px-6 py-4">
              Deposited On
            </th>
          </tr>
        </thead>
        <tbody>
          {products.map(
            (
              {
                name,
                brand,
                price: { price },
                shippable_countries,
                deposited_on,
              },
              index
            ) => {
              return (
                <tr className="border-b dark:border-neutral-500" key={index}>
                  <td className="whitespace-nowrap px-6 py-4 font-medium">
                    {index + 1}
                  </td>
                  <td className="whitespace-nowrap px-6 py-4">{name}</td>
                  <td className="whitespace-nowrap px-6 py-4">{brand}</td>
                  <td className="whitespace-nowrap px-6 py-4">{price}</td>
                  <td className="whitespace-nowrap px-6 py-4">
                    {shippable_countries.join(", ")}
                  </td>
                  <td className="whitespace-nowrap px-6 py-4">
                    {deposited_on}
                  </td>
                </tr>
              );
            }
          )}
        </tbody>
      </table>
    </div>
  );
}
