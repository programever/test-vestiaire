import { Price, Product } from "Types";

export function query(
  filtering: null | ((product: Product) => boolean),
  mapping: null | ((product: Product) => Product),
  sorting: null | ((a: Product, b: Product) => number),
  products: Product[]
): Product[] {
  const products_ =
    filtering != null && mapping == null // filter only
      ? products.filter(filtering)
      : filtering == null && mapping != null // map only
      ? products.map(mapping)
      : filtering != null && mapping != null // filter map
      ? products.reduce((acc: Product[], product: Product) => {
          if (filtering(product)) {
            acc.push(mapping(product));
          }
          return acc;
        }, [])
      : products;

  return sorting != null ? products_.sort(sorting) : products_;
}

export function filterByBrand(name: string) {
  return ({ brand }: Product): boolean => {
    return name === brand;
  };
}

export function filterByPriceRange(min: number, max: number) {
  return ({ price: { price_in_cents } }: Product): boolean => {
    return min * 100 <= price_in_cents && price_in_cents <= max * 100;
  };
}

export function filterByShippable(name: string) {
  return ({ shippable_countries }: Product): boolean => {
    return shippable_countries.includes(name);
  };
}

export function discountByPct(num: number) {
  return (product: Product): Product => {
    return { ...product, price: changePricePct(num, product.price) };
  };
}

export function reverseBrand(name: string) {
  return (product: Product): Product => {
    return name === product.brand
      ? { ...product, brand: reverseString(name) }
      : product;
  };
}

export function formatDepositedOn(product: Product): Product {
  return { ...product, deposited_on: formatYMDAgo(product.deposited_on) };
}

export function sortPriceAsc(a: Product, b: Product): number {
  return a.price.price_in_cents - b.price.price_in_cents;
}

// Internal

function changePricePct(priceInCents: number, price: Price): Price {
  const { currency, price_in_cents } = price;
  const priceInCents_ =
    priceInCents < -100
      ? 0
      : price_in_cents + (priceInCents * price_in_cents) / 100;

  return {
    currency,
    price_in_cents: priceInCents_,
    price: `${(priceInCents_ / 100).toFixed(2)}${currency}`,
  };
}

function reverseString(str: string): string {
  return str
    .split("")
    .reduce((acc: string[], s: string) => [s, ...acc], [])
    .join("");
}

// Export for testing purpose
export function formatYMDAgo(str: string): string {
  const { year, month, day } = generateYMD(str);

  if (year === 0 && month === 0 && day === 0) return "Today";

  const format = (n: number, label: string): string => {
    if (n === 0) return "";
    return `${n} ${label}${n < 2 ? "" : "s"}`;
  };

  const formatted = [
    format(year, "year"),
    format(month, "month"),
    format(day, "day"),
  ].filter((s) => s !== "");
  if (formatted.length > 1) {
    formatted.splice(formatted.length - 1, 0, "and");
  }
  formatted.push("ago");
  return formatted.join(" ");
}

/*
Concern:
- This function base on month 30 days and year = 12 * 30
- Is it a good method?
Idea:
- Get total day from input day to current day as diffDays
- Get left over day from year, this is leftOverDayFromYear, Eg: 361 days = 1 | 362 = 2
- Get left over day from month base on leftOverDayFromYear, this is leftOverDayFromMonth, Eg: 31 days = 1 | 32 = 2
- So that, we have year = diffDays / 360 
                   month = leftOverDayFromYear / 360 
                   day = leftOverDayFromMonth
*/
export function generateYMD(str: string): {
  year: number;
  month: number;
  day: number;
} {
  const current = new Date();
  const deposited = new Date(str);

  const month = 30;
  const year = 12 * month;

  const diffDays = Math.ceil(
    (current.getTime() - deposited.getTime()) / (24 * 60 * 60 * 1000)
  );

  const leftOverDayFromYear =
    diffDays >= year
      ? diffDays - Math.ceil(diffDays - (diffDays % year))
      : diffDays >= month
      ? diffDays
      : 0;

  const leftOverDayFromMonth =
    leftOverDayFromYear - Math.ceil(leftOverDayFromYear - (diffDays % month));

  return {
    year: Math.round(diffDays / year),
    month: Math.round(leftOverDayFromYear / month),
    day: leftOverDayFromMonth,
  };
}
